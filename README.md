# Class Room

## Symfony 5.1

### Bundles

 - Maker
 - Fixturtes
 - Migration
 - ORM
 - validation

## Installation

1. Clone repo
2. Create database classroom
3. install composer
4. edit .env (select correct db config)
5. execute migration
`php bin/console doctrine:migration:migrate`
6. execute demo data
`php bin/console doctrine:fixture:load`
7. run server
`php -S 127.0.0.1:8000 -t public`

### Requires

- Mysql > 5.7
- PHP > 7.2
- git

### ROUTES

    content-type: application/json
    
    class_rooms          GET      ANY      ANY    /api/class-rooms          
    class_room_get_one   GET      ANY      ANY    /api/class-room/{id}      
    class_room_create    POST     ANY      ANY    /api/class-room     
    {"name":"name","active":"ON/OFF"}    
      
    class_room_update    PUT      ANY      ANY    /api/class-room/{id}
    {"name":"name","active":"ON/OFF"}         
    
    class_room_patch     PATCH    ANY      ANY    /api/class-room/{id} 
    {"name":"name"}
    {"active":"ON/OFF"}     
    
    class_room_remove    DELETE   ANY      ANY    /api/class-room/{id}  