<?php

namespace App\Controller;

use App\Entity\ClassRoom;
use App\Repository\ClassRoomRepository;
use App\Request\ClassRoomRequest;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Crud controller for class room
 * @Route("/api/class-room")
 */
class ClassRoomRestApiController extends AbstractController
{
    /**
     * @var ClassRoomRepository $repository
     * @return JsonResponse
     * @Route("s", name="class_rooms", methods="GET")
     */
    public function lists(ClassRoomRepository $repository): JsonResponse
    {
        return $this->json([
            'classRoom' => $repository->getAll()
        ]);
    }

    /**
     * @var ClassRoom $classRoom
     * @return JsonResponse
     * @Route("/{id}", name="class_room_get_one", methods="GET")
     */
    public function getOne(ClassRoom $classRoom): JsonResponse
    {
        return $this->json([
            'classRoom' => $classRoom
        ]);
    }

    /**
     * @param ClassRoomRequest $classRoomRequest
     * @param ClassRoomRepository $repository
     * @throws ORMException
     * @throws OptimisticLockException
     * @return JsonResponse
     * @Route("", name="class_room_create", methods="POST")
     */
    public function create(ClassRoomRequest $classRoomRequest, ClassRoomRepository $repository): JsonResponse
    {
        $classRoom = $repository->create($classRoomRequest);

        return $this->json([
            'classRoom' => $classRoom
        ], Response::HTTP_CREATED);
    }

    /**
     * @param ClassRoom $classRoom
     * @param ClassRoomRequest $classRoomRequest
     * @param ClassRoomRepository $repository
     * @throws ORMException
     * @throws OptimisticLockException
     * @return JsonResponse
     * @Route("/{id}", name="class_room_update", methods="PUT")
     * @Route("/{id}", name="class_room_patch", methods="PATCH")
     */
    public function update(
        ClassRoom $classRoom,
        ClassRoomRequest $classRoomRequest,
        ClassRoomRepository $repository
    ): JsonResponse
    {
        $classRoom = $repository->update($classRoom, $classRoomRequest);

        return $this->json([
            'classRoom' => $classRoom
        ], Response::HTTP_OK);
    }

    /**
     * @param ClassRoom $classRoom
     * @param ClassRoomRepository $repository
     * @throws ORMException
     * @throws OptimisticLockException
     * @return JsonResponse
     * @Route("/{id}", name="class_room_remove", methods="DELETE")
     */
    public function delete(ClassRoom $classRoom, ClassRoomRepository $repository)
    {
        $repository->delete($classRoom);

        return $this->json([], Response::HTTP_OK);
    }
}
