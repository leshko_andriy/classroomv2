<?php

namespace App\Repository;

use App\Request\CommonRequestInterface;

/**
 * Trait for fill entity prop from request
 */
trait FillerTrait
{
    /**
     * @param $entity
     * @param CommonRequestInterface $request
     * @return mixed
     */
    private function fillProp($entity, CommonRequestInterface $request)
    {
        $methods = get_class_methods($request);
        foreach ($methods as $method) {
            if (method_exists($entity, $method) && $request->{$method}() !== NULL) {
                $entity->{str_replace('get', 'set', $method)}($request->{$method}());
            }
        }

        return $entity;
    }
}
