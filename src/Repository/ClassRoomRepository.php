<?php

namespace App\Repository;

use App\Entity\ClassRoom;
use App\Request\ClassRoomRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Repository for ClassRoomEntity
 * @method ClassRoom|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClassRoom|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClassRoom[]    findAll()
 * @method ClassRoom[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClassRoomRepository extends ServiceEntityRepository
{
    use FillerTrait;

    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ClassRoom::class);
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->createQueryBuilder('cr')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @param ClassRoomRequest $classRoomRequest
     * @return ClassRoom
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function create(ClassRoomRequest $classRoomRequest): ClassRoom
    {
        $em = $this->getEntityManager();
        $room = $this->fillProp(new ClassRoom(), $classRoomRequest);
        $em->persist($room);
        $em->flush();

        return $room;
    }

    /**
     * @param ClassRoom $classRoom
     * @param ClassRoomRequest $classRoomRequest
     * @return ClassRoom
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function update(ClassRoom $classRoom, ClassRoomRequest $classRoomRequest): ClassRoom
    {
        $em = $this->getEntityManager();
        $room = $this->fillProp($classRoom, $classRoomRequest);
        $em->flush();

        return $room;
    }

    /**
     * @param ClassRoom $classRoom
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(ClassRoom $classRoom): void
    {
        $em = $this->getEntityManager();
        $em->remove($classRoom);
        $em->flush();
    }
}
