<?php

namespace App\DataFixtures;

use App\Entity\ClassRoom;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * Demo data for class room
 */
class ClassRoomFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        for ($i=1; $i<=10; $i++) {
            $room = new ClassRoom();
            $room->setName('Room ' . $i);
            $room->setActive(false);
            $manager->persist($room);
        }

        $manager->flush();
    }
}
