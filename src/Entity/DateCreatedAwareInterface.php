<?php

namespace App\Entity;

/**
 * Interface DateAwareInterface for Entity
 */
interface DateCreatedAwareInterface
{
    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime;

    /**
     * @param \DateTime | null $created
     */
    public function setCreated(?\DateTime $created): void;
}
