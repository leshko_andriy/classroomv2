<?php

namespace App\Entity;

/**
 * Auto add to entity created
 */
trait DateCreatedTrait
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime | null $created
     */
    public function setCreated(?\DateTime $created): void
    {
        $this->created = $created;
    }
}
