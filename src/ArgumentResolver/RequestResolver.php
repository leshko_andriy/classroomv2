<?php

namespace App\ArgumentResolver;

use App\Request\CommonRequestInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Resolver for entity request
 */
class RequestResolver implements ArgumentValueResolverInterface
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * RequestResolver constructor.
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param Request $request
     * @param ArgumentMetadata $argument
     * @return bool
     * @throws \ReflectionException
     */
    public function supports(Request $request, ArgumentMetadata $argument)
    {
        $reflection = new \ReflectionClass($argument->getType());
        if ($reflection->implementsInterface(CommonRequestInterface::class)) {

            return true;
        }

        return false;
    }

    /**
     * @param Request $request
     * @param ArgumentMetadata $argument
     * @return \Generator|iterable
     */
    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        $class = $argument->getType();
        /**
         * @var CommonRequestInterface $requestEntity
         */
        $requestEntity = new $class($request);
        $validGroup = $request->getMethod() === 'PATCH' ? 'post_put_patch' : 'post_put';
        $errors = $this->validator->validate($requestEntity, null, $validGroup);
        if (count($errors) > 0) {
            throw new BadRequestHttpException((string) $errors);
        }

        yield $requestEntity;
    }
}
