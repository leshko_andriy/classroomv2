<?php

namespace App\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

/**
 * Convert error to json
 */
class JsonExceptionEventListener
{
    /**
     * @var string
     */
    private $env;

    /**
     * JsonException constructor.
     * @param string $env
     */
    public function __construct(string $env)
    {
        $this->env = $env;
    }

    /**
     * @param ExceptionEvent $event
     */
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        $code = !method_exists($exception, 'getStatusCode') ? 500 : $exception->getStatusCode();
        $event->setResponse(new JsonResponse(
            ['errors' =>  $code === 500 && $this->env === 'prod' ? 'Something went wrong' : $exception->getMessage()],
            $code
        ));
    }
}
