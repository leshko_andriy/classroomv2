<?php

namespace App\DoctrineEventSubscriber;

use App\Entity\DateCreatedAwareInterface;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * Auto set created date
 */
class DateCreatedDoctrineEventSubscriber implements EventSubscriber
{
    /**
     * @return array|string[]
     */
    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     * @throws \Exception
     */
    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getEntity();
        if ($entity instanceof DateCreatedAwareInterface) {
            $entity->setCreated(new \DateTime());
        }
    }
}
