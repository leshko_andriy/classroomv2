<?php

namespace App\Request;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Set data from request to entity
 */
class ClassRoomRequest implements CommonRequestInterface
{
    /**
     * @var string
     * @Assert\NotBlank(message="Field name can not be empty", groups={"post_put"})
     * @Assert\Length(
     *      min = 3,
     *      minMessage = "Your classroom name must be at least {{ limit }} characters long",
     *      groups={"post_put", "post_put_patch"}
     * )
     */
    private $name;

    /**
     * @var string
     * @Assert\NotBlank(message="Field active can not be empty", groups={"post_put"})
     * @Assert\Choice(
     *     {"ON", "OFF"},
     *     message = "Field active can be only 'ON' or 'OFF'",
     *     groups={"post_put", "post_put_patch"}
     * )
     */
    private $active;

    /**
     * ClassRoomRequest constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->name = $request->get('name');
        $this->active = $request->get('active');
    }

    /**
     * @return string | null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return bool | null
     */
    public function getActive(): ?bool
    {
        if ($this->active === NULL){
            return null;
        }

        return $this->active === 'ON' ? true : false;
    }
}
