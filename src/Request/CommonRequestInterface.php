<?php

namespace App\Request;

use Symfony\Component\HttpFoundation\Request;

/**
 * Common request for each entity
 */
interface CommonRequestInterface
{
    /**
     * CommonRequestInterface constructor.
     * @param Request $request
     */
    public function __construct(Request $request);
}